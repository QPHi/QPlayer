Pod::Spec.new do |s|
  s.name = '	QPlayer'
  s.version = '1.0.0'
  s.license = 'MIT'
  s.summary = 'video player in Swift, simple way to play and stream media in your iOS or tvOS app'
  s.homepage = 'https://gitlab.com/QPHi/QPlayer.git'
  s.social_media_url = 'https://gitlab.com/QPHi/QPlayer.git'
  s.authors = { '2sui' => "shuaiqi2sui@gmail.com" }
  s.source = { :git => 'https://gitlab.com/QPHi/QPlayer.git', :tag => s.version }
  s.ios.deployment_target = '9.0'
  s.tvos.deployment_target = '9.0'
  s.source_files = 'Sources/*.swift'
  s.requires_arc = true
end
